package assign.services;

import assign.domain.Meeting;
import assign.domain.Project;

public interface CourseStudentService {

	public Project addProject(Project c) throws Exception;
	
	public void updateProject(int id, Project newProject) throws Exception;
	
	public void addMeeting(int id, Meeting m) throws Exception;
	
	public Project getProject(int courseId) throws Exception;
	
	public boolean deleteProject(int projectId) throws Exception;
	
}
