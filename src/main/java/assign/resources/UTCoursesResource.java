package assign.resources;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.http.client.HttpResponseException;

import assign.domain.Meeting;
import assign.domain.Project;
import assign.services.CourseStudentService;
import assign.services.CourseStudentServiceImpl;


/* ===============================================================================
 * 		Resource Layer: input validation, error checking, response generation
 * ===============================================================================
 */

@Path("/myeavesdrop")
public class UTCoursesResource {
	
	CourseStudentService courseStudentService;
	String password;
	String username;
	String dburl;
	
	
	public UTCoursesResource(@Context ServletContext servletContext) {		
		dburl = servletContext.getInitParameter("DBURL");
		username = servletContext.getInitParameter("DBUSERNAME");
		password = servletContext.getInitParameter("DBPASSWORD");
		this.courseStudentService = new CourseStudentServiceImpl(dburl, username, password);
	}
	
	
	/* ===============================================================
	 * 					POST - Create project
	 * ===============================================================
	 */
	@POST
	@Path("/projects")
	@Consumes("application/xml")
	public Response createProject(InputStream is) throws Exception {
		
		Project proj = unMarshalProject(is);
		
		// 400 Bad Request
		if(proj.getName().equals(""))
			return Response.status(400).build();
		if(proj.getDescription().equals(""))
			return Response.status(400).build();
		
		courseStudentService.addProject(proj);
		
		// 201 Created
		return Response.created(URI.create("/projects/" + proj.getProjectId())).build();	    
	}
	
	
	/* ===============================================================
	 * 					PUT - Update project
	 * ===============================================================
	 */
	@PUT
	@Path("/projects/{id}")
	@Consumes("application/xml")
	public Response updateProject(@PathParam("id") int projectId, InputStream is) throws Exception {
		
		Project newProj = unMarshalProject(is);
		
		// 400 Bad Request
		if(newProj.getName().equals(""))
			return Response.status(400).build();
		if(newProj.getDescription().equals(""))
			return Response.status(400).build();
		
		courseStudentService.updateProject(projectId, newProj);	
		return null;
	}
	
	
	/* ===============================================================
	 * 					POST - Create meeting
	 * ===============================================================
	 */
	@POST
	@Path("/projects/{id}/meetings")
	@Consumes("application/xml")
	public Response createMeeting(@PathParam("id") int projectId, InputStream is) throws Exception {

		Meeting meeting = unMarshalMeeting(is);
		
		// 400 Bad Request
		if(meeting.getName().equals(""))
			return Response.status(400).build();
		if(meeting.getYear().equals(""))
			return Response.status(400).build();
		
		courseStudentService.addMeeting(projectId, meeting);
			 
		// 201 Created
		return Response.created(URI.create("/projects/" + projectId + "/meetings/" + meeting.getMeetingId())).build();
	}
	
	
	/* ===============================================================
	 * 					GET - Read project
	 * ===============================================================
	 */
	@GET
	@Path("/projects/{id}")
	@Consumes("application/xml")
	public StreamingOutput readProject(@PathParam("id") int projectId, InputStream is) throws Exception {
	
		final Project project = courseStudentService.getProject(projectId);
		
		// 404 Not Found
		if(project == null)
			throw new NotFoundException();
		
		return new StreamingOutput() {
        public void write(OutputStream outputStream) throws IOException, WebApplicationException {
           outputProject(outputStream, project);
        	}
		};
	}
	
	
	/* ===============================================================
	 * 					DELETE - Delete project
	 * ===============================================================
	 */
	@DELETE
	@Path("/projects/{id}")
	@Consumes("application/xml")
	public Response deleteProject(@PathParam("id") int projectId) throws Exception {
	
		boolean projectFound = courseStudentService.deleteProject(projectId);
		
		// 404 Not Found
		if(!projectFound)
			throw new NotFoundException();
		
		// 200 OK
		return Response.ok().build();
	}

	
	
	// ***************************************************************
	// ***************************************************************
	
	
	
	/* ===============================================================
	 * 						Unmarshal Project
	 * ===============================================================
	 */
	private static Project unMarshalProject(InputStream is) throws JAXBException {
	    try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Project.class);
		    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		     
		    Project proj = (Project) jaxbUnmarshaller.unmarshal( is );
		     
		    return proj;
		} catch (JAXBException jaxb) {
			jaxb.printStackTrace();
			throw new WebApplicationException();
		}
	}
	
	
	/* ===============================================================
	 * 						Unmarshal Meeting
	 * ===============================================================
	 */
	private static Meeting unMarshalMeeting(InputStream is) throws JAXBException {
	    try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Meeting.class);
		    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		    
		    Meeting meeting = (Meeting) jaxbUnmarshaller.unmarshal( is );
		    
		    return meeting; 
		} catch (JAXBException jaxb) {
			jaxb.printStackTrace();
			throw new WebApplicationException();
		}
	}
	
	
	/* ===============================================================
	 * 						Output Project
	 * ===============================================================
	 */
	protected void outputProject(OutputStream os, Project project) throws IOException {
		try { 
			JAXBContext jaxbContext = JAXBContext.newInstance(Project.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(project, os);
		} catch (JAXBException jaxb) {
			jaxb.printStackTrace();
			throw new WebApplicationException();
		}
	}
	

	/* ===============================================================
	 * 						Output Meetings
	 * ===============================================================
	 */
	protected void outputMeetings(OutputStream os, Meeting meeting) throws IOException {
		try { 
			JAXBContext jaxbContext = JAXBContext.newInstance(Project.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(meeting, os);
		} catch (JAXBException jaxb) {
			jaxb.printStackTrace();
			throw new WebApplicationException();
		}
	}
	
	
	
	@GET
	@Path("/helloworld")
	@Produces("text/html")
	public String helloWorld() {
		System.out.println("Inside helloworld");
		System.out.println("DB creds are:");
		System.out.println("DBURL:" + dburl);
		System.out.println("DBUsername:" + username);
		System.out.println("DBPassword:" + password);		
		return "Hello world " + dburl + " " + username + " " + password; 		
	}	
	
	
	
}
