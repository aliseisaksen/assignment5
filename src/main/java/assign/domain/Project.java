package assign.domain;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "project")
@XmlAccessorType(XmlAccessType.FIELD)
public class Project {
	
	
	
	@XmlElement
	private String name;
	
	@XmlElement
	private String description;
	
	@XmlAttribute
	private int project_id;
	
	@XmlElementWrapper(name="meetings")
    @XmlElement(name="meeting")
	private List<Meeting> meetings = new ArrayList<Meeting>();

	
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public int getProjectId() {
		return project_id;
	}
	
	public void setProjectId(int project_id) {
		this.project_id = project_id;
	}
	
	public List<Meeting> getMeetings() {
		return meetings;
	}
	
	public void setMeetings(List<Meeting> meetings) {
		this.meetings = meetings;
	}
}
