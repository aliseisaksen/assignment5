package assign.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "meeting") // ?
@XmlAccessorType(XmlAccessType.FIELD)
public class Meeting {
	
//	@XmlElement
	private String name; // remove 'private' ?
	
//	@XmlElement
	private String year;
	
	@XmlAttribute
	private int meeting_id;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getYear() {
		return year;
	}
	
	public void setYear(String year) {
		this.year = year;
	}
	
	public int getMeetingId() {
		return meeting_id;
	}
	
	public void setMeetingId(int meeting_id) {
		this.meeting_id = meeting_id;
	}
}
