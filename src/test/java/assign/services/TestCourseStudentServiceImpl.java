package assign.services;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import assign.domain.Meeting;
import assign.domain.Project;

public class TestCourseStudentServiceImpl {
	
	CourseStudentService csService = null;
	
	@Before
	public void setUp() {
		String dburl = "jdbc:mysql://localhost:3306/openstack_projects"; // student_courses db already created
		String dbusername = "aisaksen"; // user already created
		String dbpassword = ""; // no password is set
		csService = new CourseStudentServiceImpl(dburl, dbusername, dbpassword);
	}
	
	@Test
	public void testProjectAddition() {
		try {
			Project p = new Project();
			p.setName("Test Project 11");
			p.setDescription("Description of test project 11.");
			
			Meeting meeting = new Meeting();
			meeting.setName("meeting1");
			meeting.setYear("2017");
			meeting.setMeetingId(8);
			
			Meeting meeting2 = new Meeting();
			meeting2.setName("meeting2");
			meeting2.setYear("2018");
			meeting2.setMeetingId(9);
			
			List<Meeting> meetingList = new ArrayList<Meeting>();
			meetingList.add(meeting);
			meetingList.add(meeting2);
			p.setMeetings(meetingList);
			p = csService.addProject(p);
			
			Project c1 = csService.getProject(p.getProjectId());
			
			assertEquals(c1.getName(), p.getName());
			assertEquals(c1.getDescription(), p.getDescription());
			assertEquals(c1.getProjectId(), p.getProjectId());			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
